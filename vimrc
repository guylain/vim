set nocompatible        " be iMproved
filetype off " required!

" Setting up Vundle - the vim plugin bundler
" shamelessly nicke from github.com/fisadev
let iCanHazVundle=1
let vundle_readme=expand('~/.vim/bundle/vundle/README.md')
if !filereadable(vundle_readme)
    echo "Installing Vundle..."
    echo ""
    silent !mkdir -p ~/.vim/bundle
    silent !git clone https://github.com/gmarik/vundle ~/.vim/bundle/vundle
    let iCanHazVundle=0
endif

" Touch untracked config files
let colourme=1
let colour_vim=expand('~/.vim/colours.vim')
if !filereadable(colour_vim)
    echo "Creating colour file - declare colorscheme in colours.vim"
    echo ""
    silent !echo '"colorscheme zenburn' > ~/.vim/colours.vim
    let colourme=0
endif
let foldme=1
let folds_vim=expand('~/.vim/folds.vim')
if !filereadable(folds_vim)
    echo "Creating folds file - declare foldmethod in folds.vim"
    echo ""
    silent !echo '"foldmethod=syntax' > ~/.vim/folds.vim
    let foldme=0
endif

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

""""""""""""""""""""""
" Bundles go here: "
""""""""""""""""""""""
" let Vundle manage Vundle
" required!
Bundle 'gmarik/vundle'
Bundle 'tpope/vim-fugitive.git'
Bundle 'scrooloose/syntastic'
Bundle 'sheerun/vim-polyglot'
" Bundle 'myusuf3/numbers.vim'
Bundle '2072/PHP-Indenting-for-VIm'
Bundle 'lsdr/monokai'

""""""""""""""""""""
" Basic Settings "
""""""""""""""""""""
syntax on
filetype plugin indent on
source ~/.vim/colours.vim
source ~/.vim/folds.vim

" Turn off swap files
set noswapfile
set nobackup
set nowb
"keep undo history across sessions by storing in file
silent !mkdir ~/.vim/backups > /dev/null 2>&1
set undodir=~/.vim/backups
set undofile


" If using a dark background within the editing area and syntax highlighting
" turn on this option as well
set background=dark

" Uncomment the following to have Vim jump to the last position when
" reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

" Mac backspace key"
set backspace=eol,indent,start

" The following are commented out as they cause vim to behave a lot
" differently from regular Vi. They are highly recommended though.
set showcmd		" Show (partial) command in status line.
set showmatch		" Show matching brackets.
set ignorecase		" Do case insensitive matching
set smartcase		" Do smart case matching
set incsearch		" Incremental search
set autowrite		" Automatically save before commands like :next and :make
set hidden             	" Hide buffers when they are abandoned
set mouse=a		" Enable mouse usage (all modes)
" set relativenumber	" Show line number offset from cursor
set number		" Display line numbers
set numberwidth=1	" Use only 1 column (and 1 space) when possible
set cursorline		" Have a line indicating cursor position
set ls=2		" Always show the status line
set confirm		" Prompt if unsaved changes
set showcmd		" Show incomplete normal mode commands as I type
set report=0		" : commands always print changed line count
set shortmess+=a	" Use [+]/[RO]/[w] for modified/read-only/written
set clipboard=unnamed   " Interact with mac clipboard
